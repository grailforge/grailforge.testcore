using System.Net.Http;
using System.Threading.Tasks;
using GrailForge.TestCore.xUnit;
using Xunit;

namespace GrailForge.TestCore.Demo.Tests
{
    public class ExampleTests
    {
        [IntegrationTest]
        public async Task CanConnectToHomePage()
        {
            var result = await new HttpClient().GetAsync("https://grailforge.eu");

            Assert.True(result.IsSuccessStatusCode);
        }

        [Bug("GFXX-01234")]
        public async Task CantConnectToHomepageOverHxxp()
        {
            var result = await new HttpClient().GetAsync("hxxp://grailforge.eu");

            Assert.False(result.IsSuccessStatusCode);
        }
    }
}
