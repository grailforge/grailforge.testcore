﻿using System;
using Xunit;
using Xunit.Sdk;

namespace GrailForge.TestCore.xUnit
{
    [TraitDiscoverer(BugDiscoverer.DiscovererTypeName, DiscovererUtil.AssemblyName)]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public sealed class BugAttribute : FactAttribute, ITraitAttribute
    {
        public BugAttribute(string ticketId)
        {
            TicketId = ticketId;
        }

        public BugAttribute()
        {

        }

        public string TicketId { get; }
    }
}