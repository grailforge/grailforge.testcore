﻿using System;
using Xunit;
using Xunit.Sdk;

namespace GrailForge.TestCore.xUnit
{
    [TraitDiscoverer(IntegrationTestDiscoverer.DiscovererTypeName, DiscovererUtil.AssemblyName)]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public sealed class IntegrationTestAttribute : FactAttribute, ITraitAttribute
    {
        public const string EnvironmentVariable = "INTEGRATIONTEST";
        
        public IntegrationTestAttribute()
        {
            // TODO: work out best way to selectively activate this without dodgy manual hacks
            Skip = string.IsNullOrWhiteSpace(Environment.GetEnvironmentVariable(EnvironmentVariable))
                ? null
                : "Integration test skipped";
        }
    }
}