﻿using System;
using Xunit.Sdk;

namespace GrailForge.TestCore.xUnit
{
    [TraitDiscoverer(CategoryDiscoverer.DiscovererTypeName, DiscovererUtil.AssemblyName)]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class CategoryAttribute : Attribute, ITraitAttribute
    {
        public CategoryAttribute(string categoryName)
        {
            Name = categoryName;
        }

        public string Name { get; }
    }
}