﻿using System.Collections.Generic;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace GrailForge.TestCore.xUnit
{
    public class IntegrationTestTheoryDiscoverer : ITraitDiscoverer
    {
        internal const string DiscovererTypeName = DiscovererUtil.AssemblyName + "." + nameof(IntegrationTestTheoryDiscoverer);

        public IEnumerable<KeyValuePair<string, string>> GetTraits(IAttributeInfo traitAttribute)
        {
            yield return new KeyValuePair<string, string>("Category", "IntegrationTest");
        }
    }
}